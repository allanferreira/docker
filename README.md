/* =============
= REPOSITORIES =
============= */

dockersamples/static-site

/* =============================
= WORKING WITH CONTAINER IMAGE =
============================= */

# pull and run container
docker run CONTAINER_IMAGE

# pull and run container in background (detach mode)
docker run -d CONTAINER_IMAGE

# pull, run and open terminal
docker run -it CONTAINER_IMAGE

# name to container
docker run --name CONTAINER_NAME CONTAINER_IMAGE

# run a stopped container
docker start CONTAINER_IMAGE

# stop run container
docker stop CONTAINER_IMAGE

# stop run container with time control
docker stop -t 0 CONTAINER_IMAGE

# run and open terminal
docker start -ai CONTAINER_IMAGE

# inspect container infos
docker inspect CONTAINER_IMAGE

/* ==================
= RUN IMAGE IN PORT =
================== */

# pull and run container in background (detach mode) with random external ports
docker run -d -P CONTAINER_IMAGE

# pull and run container in background (detach mode) with external ports
docker run -d -p EXTERNAL_PORT:INTERNAL_PORT CONTAINER_IMAGE

# pull and run container in background (detach mode) with random external ports n` envariment variable
docker run -d -P -e ENV_VARNAME="VALUE" CONTAINER_IMAGE

# show vm ip to docker instalation for toolbox
docker-machine ip

# show container port 
docker port CONTAINER_IMAGE

/* =============================
= MANAGER CONTAINERS N` IMAGES =
============================= */

# List actives containers
docker ps

# List all containers
docker ps -a

# Remove container
docker rm CONTAINER_IMAGE

# Remove all stopped containers
docker container prune
docker rm $(docker ps -aq)

# List all images
docker images

# Remove image
docker rmi IMAGE


/* ====================
= WORKING WITH VOLUME =
==================== */

# create a volume (just setting destination)
docker -v "/DESTINATION_PATH/" CONTAINER_IMAGE

# create a volume with origin n` destination (/Users/.../...)
docker -v "/ORIGIN_PATH:/DESTINATION_PATH"

# create a volume with origin n`destination and work folder
docker -v "/ORIGIN_PATH:/DESTINATION_PATH" -w "/DESTINATION_PATH"
